# Descripción

Historia y conceptos básicos del sistema de control de versiones GIT

# Dependencias

[hovercraft!](https://hovercraft.readthedocs.io/en/latest/)

# Uso

```sh
$ virtualenv -p python3 charla-git
$ cd charla-git
$ source bin/activate
$ pip3 install hovercraft
$ git clone https://gitlab.com/ceyusa/charla-git.git
$ cd charla-git
$ hovercraft git.rst
....
$ deactivate
```
