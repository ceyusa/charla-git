##################
Introducción a GIT
##################

:title: Introducción a GIT
:author: Víctor Manuel Jáquez Leal
:description: Historia y conceptos básicos del sistema de control de versiones
	      GIT
:css: slides.css
:js-body: print.js
:skip-help: to

These slides have been created using `hovercraft
<https://hovercraft.readthedocs.org/>`_.

.. title:: Introducción a GIT

----

Introducción a GIT
==================

Víctor Jáquez
-------------

@ceyusa
'''''''

20 de mayo del 2019
-------------------

https://people.igalia.com/vjaquez/talks/git2019

----

¿Qué es un sistema de control de versiones?
===========================================

* Registra cambios realizados sobre un conjunto de archivos a lo largo del
  tiempo.

  * Se puede recuperar un estado anterior del archivo.

  * Permite comparar los cambios.

  * Registra *quién*, *cuándo* y *por qué* se introdujeron dichos cambios.

* **traceability**

----

¿Qué tipo de archivos se *deberían* agregar en un VCS?
======================================================

* Archivos de texto directamente manipulados por personas.

  * ¿Imágenes?
  * ¿Archivos auto-generados (binarios)?
  * ¿Documentos de MS-Word o PowerPoint?

----

Sistema de control de versiones local
=====================================

* El enfoque ingenuo: a cada cambio hacer una copia con la fecha.

* VCS local: base de datos local que registra los cambios realizados.

  * MacOS: TimeMachine
  * Unix: RCS (revision control system)

----

.. image:: local-vcs.png
   :align: center
   :width: 100%

----

Sistema de control de versiones centralizado
============================================

* ¿Cómo colaborar?

* VCS centralizado: servidor contiene todos los archivos versionados.

  * Los clientes descargan la última versión y hacen *queries* de los cambios.

* CVS, Subversion, etc.

* **single point of failure**

----

.. image:: central-vcs.png
   :align: center
   :width: 100%

----

Sistema de control de versiones distribuido
===========================================

* VCS distribuido: los clientes replican, entre sí, el repositorio *entero*.

* Estructura en red (peer-to-peer) que permite diferentes **work flows**.

* Git, Mercurial, Bazaar, Darcs, etc.

----

.. image:: distributed-vcs.png
   :align: center
   :width: 100%

----

Histora de GIT
==============

* Desarrollo del kernel Linux

  * Los colaboradores enviaban los parches por correo electrónico

  * Linus Torvalds era el sistema de control de versiones

* 2002 - BitKeeper: software comercial

* 2005 - Git: GPL2

----

Charla de Linus Torvals sobre git en Google
===========================================

https://www.youtube.com/watch?v=4XpnKHJAok8

.. raw:: html

   <center>
   <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/4XpnKHJAok8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
   </center>

----

Conceptos fundamentales
=======================

* Instantáneas (snapshot), no diferencias

.. image:: snapshots.png
   :align: center
   :width: 100%

----

Conceptos fundamentales (cont)
==============================

* Casi todas las operaciones son locales

  * *Queries* locales

* Integridad interna

  * Cada objeto (snapshot) está asociado a su *checksum* (SHA-1)

    * Si un archivo cambia, su *checksum* se invalida

----

Los tres estados posibles de los archivos
=========================================

* Modified

* Staged

* Committed

----

.. image:: object-states.png
   :align: center
   :width: 100%

----

Repositorios
============

**Inicializar un repositorio en un directorio existente**

.. code:: console

   $ mkdir my-project
   $ cd my-project
   $ git init

.. code:: console

   $ git add .
   $ git commit -m "versión inicial"

----

Repositorios
============

**Clonar un repositorio existente**

.. code:: console

   $ git clone https://gitlab.com/ceyusa/charla-git.git

----

Ciclo de vida de un archivo
===========================

.. image:: file-life-cycle.png
   :align: center
   :width: 100%

----

*status* de los archivos
========================

.. code:: console

   $ git status

   # On branch master
   nothing to commit, working directory clean

----

*status* de los archivos
========================

.. code:: console

   $ touch README
   $ git status

   # On branch master
   # Untracked files:
   #   (use "git add <file>..." to include in what will be committed)
   #
   #   README
   nothing added to commit but untracked files present (use "git add" to track)

----

*status* de los archivos
========================

.. code:: console

   $ git add README
   $ git status
   # On branch master
   # Changes to be committed:
   #   (use "git reset HEAD <file>..." to unstage)
   #
   #   new file:   README
   #

----

Comandos básicos
================

* Mostrar diferencias

.. code:: console

   $ git diff

* Confirmar cambios

.. code:: console

   $ git commit

* Mostrar histórico

.. code:: console

   $ git log

----

Comandos básicos
================

* Eliminar archivos

.. code:: console

   $ git rm README

* Mover archivos

.. code:: console

   $ git mv README README.md


----

Recomendaciones para commits
============================

* Los *commits* en git son *baratos*

* Un cambio lógico → Un commit

  * Es más fácil de revisar e identificar cuándo se introdujo un *bug*

----

Recomendaciones para logs
=========================

* Primer parráfo: 50 caracteres y con prefijo

.. code:: console

   doc: Add new parameters in the manual

* Siguientes párrafos 75 carateres

* Responder a las preguntas

  * ¿Por qué la modificación es necesaria?

  * ¿Cómo está implementada la solución?

  * ¿Cuáles son los detalles de implementación importantes?

  * ¿Qué resultados son esperados con este cambio?

https://www.ozlabs.org/~akpm/stuff/tpp.txt

----

Ramas
=====

* Tomar la rama principal (master) y a partir de allí continuar trabajando

  * El *branching* en git es muy *barato* computacionalmente

  * Moverse entre distintas ramas también es fácil y *barato*

.. image:: branch-and-history.png
   :align: center
   :width: 100%

----

Branching workflow
==================

* Local

  * Cada *feature* que se implementa es una rama (*work-in-progress*)

  * Fácil de enviar a revisión una rama con un conjunto de cambios

  * Ramas de corta duración

* Remota

  * **master** - Rama principal

  * **1.0**, **2.1**, … - Rama que representa una versión liberada (*release*)

  * Ramas de larga duración

Comandos básicos
================

* Crear una rama

.. code:: console

   $ git branch nombre-rama

* Cambiar (head) a una rama

.. code:: console

   $ git checkout nombre-rama

* Borrar una rama (¡cuidado!)

.. code:: console

   $ git branch -D nombre-rama

----

Fusionar ramas
==============

#. Obtienes *master*

#. Creas una rama y comienzas a trabajar

#. Terminas de implementar algo, vuelves obtener *master* ¡y ha cambiado!

.. code:: console

   $ git checkout -b bug31521
     [hack hack hack]
   $ git checkout master
   $ git pull
     [¡Aparecen cambios!]
   $ git merge bug31521

----

.. image:: topic-branches-1.png
   :align: center
   :width: 100%

----

.. image:: topic-branches-2.png
   :align: center
   :width: 100%

----

.. image:: git-tree.png
   :align: center
   :width: 80%
